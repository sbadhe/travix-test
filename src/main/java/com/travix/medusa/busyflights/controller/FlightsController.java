package com.travix.medusa.busyflights.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.service.FlightsService;

/**
 * 
 * Rest controller for Busy Fight Search
 * 
 * @author Sagar Badhe
 *
 */
@RestController
@RequestMapping("/busyflights")
public class FlightsController {

	private static final Logger LOGGER = LoggerFactory.getLogger(FlightsController.class);

	@Autowired
	private FlightsService flightsService;

	/**
	 * Search for the fights based on User input like source, destination,
	 * start, return
	 *
	 * @param person
	 *            Object which is to be in DB
	 * @return Status code and message.
	 */
	@RequestMapping(value = "/searchFights", method = RequestMethod.GET, consumes = MediaType.APPLICATION_JSON_VALUE)
	public @ResponseBody BusyFlightsResponse searchFights(@RequestBody BusyFlightsRequest busyFlightsRequest) {

		return flightsService.findFlights(busyFlightsRequest);
	}

}
