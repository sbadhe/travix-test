package com.travix.medusa.busyflights.domain.busyflights;

import java.util.List;

import com.travix.medusa.busyflights.model.Flight;

public class BusyFlightsResponse {
	
	private List<Flight> flights;
	private int responceStatus;
	private String description;
	
	public List<Flight> getFlights() {
		return flights;
	}
	public void setFlights(List<Flight> flights) {
		this.flights = flights;
	}
	public int getResponceStatus() {
		return responceStatus;
	}
	public void setResponceStatus(int responceStatus) {
		this.responceStatus = responceStatus;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	
	
	
}
