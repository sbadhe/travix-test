package com.travix.medusa.busyflights.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.integration.ToughJetClient;
import com.travix.medusa.busyflights.service.ToughJetService;

/**
 * @author a550207
 *
 */
public class ToughJetServiceImpl implements ToughJetService {

	@Autowired
	private ToughJetClient toughJetApiClient;

	@Override
	public List<ToughJetResponse> getFightDetails(ToughJetRequest toughJetRequest) {

		return toughJetApiClient.getFights(toughJetRequest);
	}
}
