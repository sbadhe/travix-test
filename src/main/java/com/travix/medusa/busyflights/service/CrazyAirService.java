package com.travix.medusa.busyflights.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;

@Service
public interface CrazyAirService {

   List<CrazyAirResponse> getFightDetails(final CrazyAirRequest crazyAirRequest);
}
