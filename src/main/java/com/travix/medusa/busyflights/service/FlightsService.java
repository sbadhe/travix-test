package com.travix.medusa.busyflights.service;

import org.springframework.stereotype.Service;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;

/**
 * 
 * Flight Service to get all fight details
 * 
 * @author Sagar Badhe
 *
 */
@Service
public interface FlightsService {

	BusyFlightsResponse findFlights(final BusyFlightsRequest busyFlightsRequest);
}
