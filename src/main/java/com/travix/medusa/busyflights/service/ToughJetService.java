package com.travix.medusa.busyflights.service;

import java.util.List;

import org.springframework.stereotype.Service;

import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

/**
 * 
 * Server Implentation.
 * 
 * @author Sagar Badhe
 *
 */
@Service
public interface ToughJetService {

	List<ToughJetResponse> getFightDetails(final ToughJetRequest toughJetRequest);

}
