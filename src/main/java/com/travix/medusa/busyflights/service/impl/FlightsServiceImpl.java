package com.travix.medusa.busyflights.service.impl;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsRequest;
import com.travix.medusa.busyflights.domain.busyflights.BusyFlightsResponse;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.model.Flight;
import com.travix.medusa.busyflights.service.CrazyAirService;
import com.travix.medusa.busyflights.service.FlightsService;
import com.travix.medusa.busyflights.service.ToughJetService;
import com.travix.medusa.busyflights.utils.MergeAndSortFightUtil;

/**
 * 
 * fight Service implementation.
 * 
 * @author Sagar Badhe
 *
 */
public class FlightsServiceImpl implements FlightsService {

	@Autowired
	private CrazyAirService crazyAirService;

	@Autowired
	private ToughJetService toughJetServic;

	@Override
	public BusyFlightsResponse findFlights(final BusyFlightsRequest busyFlightsRequest) {

		final CrazyAirRequest crazyAirRequest = new CrazyAirRequest();
		crazyAirRequest.setDepartureDate(busyFlightsRequest.getDepartureDate());
		crazyAirRequest.setReturnDate(busyFlightsRequest.getReturnDate());
		crazyAirRequest.setOrigin(busyFlightsRequest.getOrigin());
		crazyAirRequest.setDestination(busyFlightsRequest.getDestination());
		crazyAirRequest.setPassengerCount(busyFlightsRequest.getNumberOfPassengers());

		List<CrazyAirResponse> listOfFightFromCrayAir = crazyAirService.getFightDetails(crazyAirRequest);

		final ToughJetRequest toughJetRequest = new ToughJetRequest();
		toughJetRequest.setFrom(busyFlightsRequest.getOrigin());
		toughJetRequest.setTo(busyFlightsRequest.getDestination());
		toughJetRequest.setInboundDate(busyFlightsRequest.getReturnDate());
		toughJetRequest.setOutboundDate(busyFlightsRequest.getDepartureDate());
		toughJetRequest.setNumberOfAdults(busyFlightsRequest.getNumberOfPassengers());

		List<ToughJetResponse> listOfFightFromToughJet = toughJetServic.getFightDetails(toughJetRequest);

		return this.getSortedFightList(listOfFightFromCrayAir, listOfFightFromToughJet);
	}

	/**
	 * Merge and Sort selected flight Details.
	 * 
	 * @param listOfFightFromCrayAir
	 * @param listOfFightFromToughJet
	 * @return
	 */
	private BusyFlightsResponse getSortedFightList(List<CrazyAirResponse> listOfFightFromCrayAir,
			List<ToughJetResponse> listOfFightFromToughJet) {
		final BusyFlightsResponse busyFlightsResponse = new BusyFlightsResponse();
		final List<Flight> listOfFight = new ArrayList<>();

		listOfFightFromCrayAir.forEach(fight -> {
			listOfFight.add(MergeAndSortFightUtil.getCrazyFlightDetail(fight));
		});
		listOfFightFromToughJet.forEach(fight -> {
			listOfFight.add(MergeAndSortFightUtil.getToughFlightDetail(fight));
		});

		if (!listOfFight.isEmpty()) {
			Collections.sort(listOfFight, (f1, f2) -> new Double(f1.getFare()).compareTo(new Double(f2.getFare())));
			busyFlightsResponse.setFlights(listOfFight);
			busyFlightsResponse.setResponceStatus(1);
			busyFlightsResponse.setDescription("All fights are sorted by prise ");

		} else {
			busyFlightsResponse.setResponceStatus(1);
			busyFlightsResponse.setDescription("No flights available for this date.");
		}

		return busyFlightsResponse;

	}

}
