package com.travix.medusa.busyflights.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.integration.CrazyAitClient;
import com.travix.medusa.busyflights.service.CrazyAirService;

@Component
public class CrazyAirServiceImpl implements CrazyAirService {

	@Autowired
	private CrazyAitClient CrazyAitClient;

	@Override
	public List<CrazyAirResponse> getFightDetails(CrazyAirRequest crazyAirRequest) {

		return CrazyAitClient.getFights(crazyAirRequest);
	}
}
