package com.travix.medusa.busyflights.integration;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirRequest;
import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;

/**
 * Crazy Jet API client.
 *
 * It will call the API from Crazy and will get Response in JSON form.
 */
public class CrazyAitClient extends BaseClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(CrazyAitClient.class);

	public CrazyAitClient(RestTemplate template, String baseUrl) {
		this.template = template;
		this.baseUrl = baseUrl;
	}

	/**
	 * @param CrazyAirRequest
	 * @return List of fights details from CrazyAir
	 */
	public List<CrazyAirResponse> getFights(final CrazyAirRequest crazyAirRequest) {
		try {
			ParameterizedTypeReference<List<CrazyAirResponse>> type = new ParameterizedTypeReference<List<CrazyAirResponse>>() {
			};
			ResponseEntity<List<CrazyAirResponse>> instance = template.exchange(baseUrl, HttpMethod.GET, null, type,
					crazyAirRequest.getOrigin(), crazyAirRequest.getDestination(), crazyAirRequest.getDepartureDate(),
					crazyAirRequest.getReturnDate(), crazyAirRequest.getPassengerCount());
			if (instance.getStatusCode() == HttpStatus.OK)
				return instance.getBody();
			else
				return Collections.emptyList();
		} catch (Exception e) {
			LOGGER.error("error Unable find fights ", e);
			return Collections.emptyList();
		}
	}
}
