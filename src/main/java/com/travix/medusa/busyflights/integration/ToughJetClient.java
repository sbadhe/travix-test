package com.travix.medusa.busyflights.integration;

import java.util.Collections;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.travix.medusa.busyflights.domain.toughjet.ToughJetRequest;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;

/**
 * Tough Jet API client.
 *
 * It will call the API from ToughJet and will get Response in JSON form.
 */
public class ToughJetClient extends BaseClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(ToughJetClient.class);

	public ToughJetClient(RestTemplate template, String baseUrl) {
		this.template = template;
		this.baseUrl = baseUrl;
	}

	/**
	 * @param toughJetRequest
	 * @return List of fights details from ToughJet
	 */
	public List<ToughJetResponse> getFights(final ToughJetRequest toughJetRequest) {
		try {
			ParameterizedTypeReference<List<ToughJetResponse>> type = new ParameterizedTypeReference<List<ToughJetResponse>>() {
			};
			ResponseEntity<List<ToughJetResponse>> instance = template.exchange(baseUrl, HttpMethod.GET, null, type,
					toughJetRequest.getFrom(), toughJetRequest.getTo(), toughJetRequest.getInboundDate(),
					toughJetRequest.getOutboundDate(), toughJetRequest.getNumberOfAdults());
			if (instance.getStatusCode() == HttpStatus.OK)
				return instance.getBody();
			else
				return Collections.emptyList();
		} catch (Exception e) {
			LOGGER.error("error Unable find fights ", e);
			return Collections.emptyList();
		}
	}
}
