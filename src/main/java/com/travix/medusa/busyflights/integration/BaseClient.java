package com.travix.medusa.busyflights.integration;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.web.client.RestTemplate;

public abstract class BaseClient {

	private static final Logger LOGGER = LoggerFactory.getLogger(BaseClient.class);

	protected RestTemplate template;
	protected String baseUrl;

	protected <T> HttpEntity<T> getHttpEntity(T dto) {
		HttpHeaders httpHeaders = new HttpHeaders();
		httpHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));
		httpHeaders.setContentType(MediaType.APPLICATION_JSON);
		return new HttpEntity<>(dto, httpHeaders);
	}

}
