package com.travix.medusa.busyflights.utils;

import java.text.DecimalFormat;

import com.travix.medusa.busyflights.domain.crazyair.CrazyAirResponse;
import com.travix.medusa.busyflights.domain.toughjet.ToughJetResponse;
import com.travix.medusa.busyflights.model.Flight;

public class MergeAndSortFightUtil {

	private static final DecimalFormat df2 = new DecimalFormat(".##");



	public static final Flight getCrazyFlightDetail(final CrazyAirResponse crazyAirResponse) {
		final Flight flight = new Flight();
		flight.setAirline(crazyAirResponse.getAirline());
		flight.setSupplier("CRAZYAIR"); // Add in constants
		flight.setArrivalDate(crazyAirResponse.getArrivalDate());
		flight.setDepartureAirportCode(crazyAirResponse.getDepartureAirportCode());
		flight.setDestinationAirportCode(crazyAirResponse.getDestinationAirportCode());
		flight.setFare(Double.parseDouble(df2.format(crazyAirResponse.getPrice())));
		return flight;
	}

	public static final Flight getToughFlightDetail(final ToughJetResponse toughJetResponse) {
		final Flight flight = new Flight();
		flight.setAirline(toughJetResponse.getCarrier());
		flight.setSupplier("ToughJET"); // Add in constants
		flight.setArrivalDate(toughJetResponse.getInboundDateTime());
		flight.setDepartureAirportCode(toughJetResponse.getOutboundDateTime());
		flight.setDestinationAirportCode(toughJetResponse.getDepartureAirportName());
		final double totalPrise = toughJetResponse.getBasePrice() + toughJetResponse.getTax()
				- toughJetResponse.getDiscount();
		flight.setFare(Double.parseDouble(df2.format(totalPrise)));
		return flight;
	}

}
